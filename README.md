# taller_gitlab_club_nutresa mcmartinez

Este documento presenta la descripción de la solución y la descripcion de la organizacion de la carpetas del proyecto del Taller de Buenas Practicas en Soluciones Analiticas Parte 2

## Descripción de la solución

### Objetivo:
Medir y representar el rendimiento interno de la organización Alianza CAOBA

### Descripción Indicadores de Negocio:

Preguntas:

Como medir lo que importa ?
Como asegurarse que no son solo activades lo que se describe ?

Indicadores de Negocio:

Usando indicadores que relacionan objetivos y resultados (OKRs) y no indicadores de desempeño (KPIs) se puede establecer una relacion entre actividades en cada sprint con los objetivos de la organización

## Descripción de organización carpetas (Sistema de Archivos)

Las carpetas estan organizadas bajo una carpeta raiz denominada orchestration:

-orchestration/
	-data/
	-etl/
	-modelos/
	-dashboards